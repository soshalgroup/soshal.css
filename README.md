# soshal.css

This is the main soshal.css framework.

Download the latest release from the Downloads section and extract the files in
your sass folder within your project.

If you're using [soshal.wp](https://bitbucket.org/soshalgroup/soshal.wp) create
a `sass` folder inside your theme and extract the contents there. The
[soshal.wp](https://bitbucket.org/soshalgroup/soshal.wp) theme comes with a gulpfile that will work nicely with it.
