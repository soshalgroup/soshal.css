/**
 * gulpfile.js
 *
 * Runs dev and dist tasks for this project.
 */





/* --------------------------------- *
 * Gulp configuration                *
 * --------------------------------- */
var fs          = require('fs');
var path        = require('path');

var gulp        = require('gulp');
var plugins     = require('gulp-load-plugins')();
var runSequence = require('run-sequence');
var chalk       = require('chalk');

var pkg         = require('./package.json');
var dirs        = pkg['soshal.css-config'].directories;

// Options for plumber.
var plumber_options = {
    errorHandler: errorHandler
};





/* --------------------------------- *
 * Helper functions                  *
 * --------------------------------- */
// Error handler prevents Gulp from breaking on errors.
function errorHandler(err) {

    console.log('  ' + chalk.red('\u2219  errors ' + chalk.white('in ' + chalk.gray(err.plugin))));
    console.log('  ' + chalk.red('\u2219  ') + chalk.white(err.message) + ' on ' + chalk.gray('line ' + err.lineNumber) + chalk.white(' in file:'));
    console.log('  ' + chalk.red('\u2219  ') + chalk.white(err.fileName));

    this.emit('end');

}





/* --------------------------------- *
 * Dist tasks                        *
 * --------------------------------- */
// Archive directory creation.
gulp.task('archive:create_dir', function() {

    console.log('');
    console.log('  ' + chalk.cyan('\u2219  ') + chalk.white('running ' + chalk.gray('archive:create_dir')));
    console.log('');

    fs.mkdirSync(dirs.archive, '0755');

});

// Archive dist files.
gulp.task('archive:zip', function(done) {

    console.log('');
    console.log('  ' + chalk.cyan('\u2219  ') + chalk.white('running ' + chalk.gray('archive:zip')));
    console.log('');

    return gulp.src('**/*.*', { 'cwd': dirs.dist })
        .pipe(plugins.zip(pkg.name + '_v' + pkg.version + '.zip'))
        .pipe(gulp.dest('archive'));

});

// Cleanup archive directory before dist.
gulp.task('clean', function(done) {

    console.log('');
    console.log('  ' + chalk.cyan('\u2219  ') + chalk.white('running ' + chalk.gray('clean')));
    console.log('');

    require('del')([
        dirs.archive
    ], done);

});





/* --------------------------------- *
 * Dev tasks                         *
 * --------------------------------- */
// Main dev watcher.
gulp.task('dev', function() {

    console.log('');
    console.log('  ' + chalk.cyan('\u2219  ') + chalk.white('starting ' + chalk.gray('dev')));
    console.log('');

    gulp.watch(dirs.dev + '/**/*.scss', ['css:dev']);

});



// Parse Sass and build sourcemaps.
gulp.task('css:dev', function() {

    console.log('');
    console.log('  ' + chalk.cyan('\u2219  ') + chalk.white('running ' + chalk.gray('sass:dev')));
    console.log('');

    return gulp.src(dirs.dev + '/**/*.scss')
        .pipe(plugins.plumber(plumber_options))
        .pipe(plugins.sourcemaps.init())
        .pipe(plugins.sass({style: 'expanded'}))
        .pipe(gulp.dest('./test'))
        .pipe(plugins.sourcemaps.write('./maps'))
        .pipe(gulp.dest('./test'));

});





/* --------------------------------- *
 * Sequenced dist tasks              *
 * --------------------------------- */
// Cleans up the archive directory and creates a new versioned archive.
gulp.task('archive:dist', function(done) {

    console.log('');
    console.log('  ' + chalk.cyan('\u2219  ') + chalk.white('starting ' + chalk.gray('dist')));
    console.log('');

    runSequence(
        'clean',
        'archive:create_dir',
        'archive:zip',
    done);

});





/* --------------------------------- *
 * Gulp commands                     *
 * --------------------------------- */
gulp.task('default', ['dev']);
gulp.task('dist', ['archive:dist']);
